import React from "react";

const AboutMe = ({ aboutMe }) => {
  return (
    <div className="aboutMe-card">
      {aboutMe.map((item) => {
        return (
          <div key={JSON.stringify(item)}>
            <p>{item.info}</p>
          </div>
        );
      })}
    </div>
  );
};

export default AboutMe;
