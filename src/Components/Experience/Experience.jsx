import React from "react";

const Experience = ({ experience }) => {
  return (
    <div>
      <div className="experience-card">
        {experience.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>{item.name}</h4>
              <h5>{item.where}</h5>
              <p>{item.date}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;
