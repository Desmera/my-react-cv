import React from "react";

const More = ({ languages, habilities, volunteer }) => {
  return (
    <div>
      <div className="languages-card">
        <h4>Langueges</h4>
        {languages.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h5>{item.language}</h5>
              <p>{item.wrlevel}</p>
              <p>{item.splevel}</p>
            </div>
          );
        })}
      </div>

      <div className="habilities-card">
        <h4>Skills</h4>
        {habilities.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p>{item}</p>
            </div>
          );
        })}
      </div>

      <div className="volunteer-card">
        <h4>Volunteer Activities</h4>
        {volunteer.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h5>{item.name}</h5>
              <h6>{item.where}</h6>
              <p>{item.date}</p>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default More;
