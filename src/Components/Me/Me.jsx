import React from "react";
import "../../Styles/Styles.css";

const Me = ({ me }) => {
  return (
    <div className="me">
      <img
        src="https://scontent.fopo2-2.fna.fbcdn.net/v/t31.18172-8/19055893_1433674486717643_4369360945601355332_o.jpg?_nc_cat=100&ccb=1-5&_nc_sid=174925&_nc_ohc=Xua2th8ZHQQAX_6pVoJ&_nc_ht=scontent.fopo2-2.fna&oh=5a3fafdc7ec4b40974bd954359438939&oe=61A5D9D5"
        alt="Mariana Perfil"
      />
      <div className="card">
        <h2>{me.name}</h2>
        {/* <h6>{me.adress}</h6> */}
        <h4>{me.city} </h4>
        {/* <p>{me.birthDate}</p> */}
        {/* <p>{me.email}</p>
        <p>{me.phone}</p> */}
      </div>
    </div>
  );
};

export default Me;
