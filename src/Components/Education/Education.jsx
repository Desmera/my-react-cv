import React from "react";

const Education = ({ education }) => {
  return (
    <div>
      <div className="education-card">
        {education.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <div>
                <h4 className="name">{item.name}</h4>
              </div>
              <div>
                <h5>{item.where}</h5>
                <p>{item.date}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Education;
