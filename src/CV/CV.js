export const CV = {
  me: {
    name: "Mariana Jesus",
    birthDate: "03/03/1997",
    adress: "Avenida Avelar Machado",
    city: "Abrantes, Portugal",
    email: "mariana_jesus1220@hotmail.com",
  },
  aboutMe: [
    {
      info: "I am a Product Designer.",
    },
    {
      info: "Who loves pets, making cakes, and camping.",
    },
  ],
  education: [
    {
      name: "Front-End Bootcamp",
      date: "30/09/2021 - to present",
      where: "Upgrade-Hub",
    },
    {
      name: "Master in Engineering and Product Design",
      date: "09/2018 - 02/2021",
      where: "University of Aveiro, Aveiro",
    },
    {
      name: "Bachelor in Design",
      date: "09/2015 - 06/2018",
      where: "University of Aveiro, Aveiro",
    },
  ],
  experience: [
    {
      name: "Extracurricular Activities",
      date: "04/2021 - 07/2021",
      where: "Basic Shcool of Alvega, Abrantes",
    },
    {
      name: "Employee in a Print Shop",
      date: "09/2019 - 03/2021",
      where: "CopyFast, Aveiro",
    },
    {
      name: "Cashier in a Supermarket",
      date: "06/2019 - 09/2019",
      where: "Pingo Doce, Abrantes",
    },
    {
      name: "Cashier in a Supermarket",
      date: "06/2018 - 09/2018",
      where: "Pingo Doce, Abrantes",
    },
  ],
  languages: [
    {
      language: "Portuguese",
      wrlevel: "writing: Native",
      splevel: "speaking: Native",
    },
    {
      language: "English",
      wrlevel: "writing: Conversational",
      splevel: "speaking: Conversational",
    },
  ],
  habilities: [
    "Solidworks",
    "Keyshot",
    "Adobe Illustrator",
    "Adobe Photoshop",
    "Adobe Illustrator",
    "Autodesk Fusion 360",
  ],
  volunteer: [
    {
      name: "ESTA",
      where: "University of Aveiro",
      date: "2017 - 2019",
      description: `Organization and planning of ESTA
                  (Espétaculo Solidário de Talentos de
                  Aveiro - solidarity talent show).`,
    },
    {
      name: "Children Shelter",
      where: "Temporary Reception Center, Abrantes",
      date: "2017 - 2016",
      description: `I encouraged children at risk at the
                  Temporary Reception Center in
                  Alferrarede, Abrantes.`,
    },
    {
      name: "Elder Home",
      where: "Elder Home, Abrantes",
      date: "2016 - 2015",
      description: `I organized activities at an Elder Home,
                  in Abrantes.`,
    },
  ],
};
