import React, { useState } from "react";
import "./Styles/Styles.css";
import Me from "./Components/Me/Me";
import Education from "./Components/Education/Education";
import Experience from "./Components/Experience/Experience";
import More from "./Components/More/More";
import { CV } from "./CV/CV";

const { me, education, experience, languages, habilities, volunteer } = CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);

  return (
    <div className="app">
      <div
        className="mainContent"
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(2, 1fr)",
          gridGap: 20,
        }}
      >
        <div>
          <Me me={me} />
        </div>

        <div>
          <div>
            {showEducation ? (
              <Education education={education} />
            ) : (
              <Experience experience={experience} />
            )}
          </div>

          <button
            className="custom-btn btn-4"
            onClick={() => setShowEducation(true)}
          >
            Education
          </button>
          <button
            className="custom-btn btn-4"
            onClick={() => setShowEducation(false)}
          >
            Experience
          </button>

          <More
            languages={languages}
            habilities={habilities}
            volunteer={volunteer}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
